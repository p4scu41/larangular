<!DOCTYPE html>
<html lang="es" ng-app="mainApp">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap  -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" type="text/css" rel="stylesheet">
        <!--  Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <!-- AngularMotion -->
        {!!Html::style('vendor/angular-motion/angular-motion.min.css')!!}
        <!-- Titip -->
        {!!Html::style('vendor/titip/titip.min.css')!!}
        <!-- BootstrapAdditions -->
        {!!Html::style('vendor/bootstrap-additions/bootstrap-additions.min.css')!!}
        <!-- jAlert -->
        {!!Html::style('vendor/jalert/jAlert-v3.css')!!}
        <!-- angular-loading-bar -->
        <link href="//cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.7.1/loading-bar.min.css" type="text/css" rel="stylesheet">
        <!-- Main App CSS -->
        {!!Html::style('css/app.min.css')!!}
        
        <title></title>
    </head>
    
    <body>
        <header class="container">
            <nav class="navbar navbar-default" role="navigation" bs-navbar>
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#/">Logo</a>
                    </div>
            
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse" ng-controller="AuthController as auth" ng-cloak>
                        <ul class="nav navbar-nav">
                            <li data-match-route="/$"><a href="#/"><i class="fa fa-home"></i> Inicio</a></li>
                            <li data-match-route="/users" ng-if="auth.isAuthenticated()"><a href="#/users"><i class="fa fa-users"></i> Usuarios</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-animation="am-flip-x" data-placement="bottom-right" bs-dropdown 
                                aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Usuario <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li ng-if="auth.isAuthenticated()"><a href="#"><i class="fa fa-credit-card"></i> Perfil</a></li>
                                    <li data-match-route="/auth" ng-if="!auth.isAuthenticated()"><a href="#@{{auth.config.url_login}}"><i class="fa fa-key"></i> Iniciar Sesión</a></li>
                                    <li ng-if="auth.isAuthenticated()" role="separator" class="divider"></li>
                                    <li ng-if="auth.isAuthenticated()"><a href="#" ng-click="auth.logout()"><i class="fa fa-power-off"></i> Salir</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>
        </header>

        <section class="main container">
            <!-- ngRoute -->
            <!-- <ng-view></ng-view> -->
            
            <!-- ui.router -->
            <div ui-view class="am-fade-and-slide-left"></div>

        </section>

        <footer class="container">
            <div class="well">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias quisquam, perspiciatis eveniet unde, incidunt fuga quod ad suscipit quis iste id laudantium ratione iure eum praesentium repudiandae provident atque blanditiis.
            </div>
        </footer>

        <!--  jQuery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.0/jquery.min.js" type="text/javascript"></script>
        <!-- AngularJS -->
        <script src="https://code.angularjs.org/1.5.0/angular.min.js" type="text/javascript"></script> 
        <script src="https://code.angularjs.org/1.5.0/angular-route.min.js" type="text/javascript"></script> 
        <script src="https://code.angularjs.org/1.5.0/angular-messages.min.js" type="text/javascript"></script> 
        <script src="https://code.angularjs.org/1.5.0/angular-touch.min.js" type="text/javascript"></script> 
        <script src="https://code.angularjs.org/1.5.0/angular-animate.min.js" type="text/javascript"></script> 
        <script src="https://code.angularjs.org/1.5.0/i18n/angular-locale_es-mx.js" type="text/javascript"></script>
        <!-- Underscore -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" type="text/javascript"></script>
        <!-- Restangular -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/restangular/1.5.2/restangular.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Moment -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js" type="text/javascript"></script>
        <!-- Angular UI Bootstrap -->
        {!!Html::script('/vendor/ui-bootstrap/ui-bootstrap-tpls-1.1.2.min.js')!!}
        <!-- AngularStrap -->
        {!!Html::script('vendor/angular-strap/angular-strap.min.js')!!}
        {!!Html::script('vendor/angular-strap/angular-strap.tpl.min.js')!!}
        <!-- Satellizer -->
        <!--[if lte IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/Base64/0.3.0/base64.min.js" type="text/javascript"></script>
        <![endif]-->
        <script src="//cdn.jsdelivr.net/satellizer/0.13.4/satellizer.min.js" type="text/javascript"></script>
        <!-- AngularUI Router -->
        {!!Html::script('vendor/angular-ui-router/angular-ui-router.min.js')!!}
        <!-- JSON-js -->
        {!!Html::script('vendor/json-js/json2.js')!!}
        <!-- JavaScript Cookie -->
        {!!Html::script('vendor/js.cookie/js.cookie.js')!!}
        <!-- jQuery Storage API -->
        {!!Html::script('vendor/jquery.storageapi/jquery.storageapi.min.js')!!}
        <!-- jAlert -->
        {!!Html::script('vendor/jalert/jAlert-v3.min.js')!!}
        {!!Html::script('vendor/jalert/jAlert-functions.min.js')!!}
        <!-- angular-loading-bar -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.7.1/loading-bar.min.js" type="text/javascript"></script>
        <!-- App -->
        {!!Html::script('/js/config.min.js')!!}
        {!!Html::script('/js/helper.min.js')!!}
        {!!Html::script('/js/mainApp.min.js')!!}
        {!!Html::script('/js/services.min.js')!!}
        {!!Html::script('/js/controllers.min.js')!!}
    </body>
</html>