var Config = {
    /**
     * Main name of the Angular Module, it has to be set in the HTML
     * @type string
     */
    appName: 'mainApp',
    /**
     * Determine if the messages of errors will be show in console
     *
     * @type boolean
     */
    debug: true,

    /**
     * Ajax requests
     * @type {Object}
     */
    REQUEST: {
        GET: 'get',
        POST: 'post',
        DELETE: 'delete',
        PUT: 'put',
        PATCH: 'patch',
    },

    /**
     * Methods executed by Laravel
     * @type {Object}
     */
    ACTION: {
        INDEX: 'index',
        CREATE: 'create',
        STORE: 'store',
        SHOW: 'show',
        EDIT: 'edit',
        UPDATE: 'update',
        DESTROY: 'destroy',
    },

    /**
     * Home URL of the web site
     *
     * @type string
     */
    url_host: 'http://localhost/larangular/public/api/v1',

    /**
     * Home URL of the web site
     *
     * @type string
     */
    url_web: 'http://localhost/larangular/public',

    /**
     * Path login view in the frontend
     *
     * @type string
     */
    url_login: '/auth',

    /**
     * Path login to the route in the backend
     *
     * @type string
     */
    url_auth: '/authenticate',

    /**
     * Load indicator icon
     *
     * @type string
     */
    icon_spinner: '<i class="fa fa-spinner fa-pulse fa-lg"></i>',

    /**
     *  CSS Selector of the load indicator icon
     *
     * @type string
     */
    selector_icon_spinner: 'i.fa.fa-spinner',
};