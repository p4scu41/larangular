'use strict';
/* globals mainApp */

/**
 * Users Service Provider
 */
mainApp.service('UsersService', ['Restangular', function(Restangular){
    return Restangular.service('users');
}]);