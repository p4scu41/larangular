'use strict';
/* globals angular, Config, Helper, _, alert, confirm, console */

/**
 * @ngdoc function
 * @name mainApp.controllers:UserController
 * @description
 * # UserController
 * Controller of the mainApp
 */
angular.module(Config.appName)
    .controller('UserController', ['$scope', 'UsersService', '$uibModal', 'appConfig', '$auth', '$state', '$alert',
        function ($scope, UsersService, $uibModal, appConfig, $auth, $state, $alert) {
            if (!$auth.isAuthenticated()) {
                $alert({
                    title: 'Acceso denegado', 
                    content: 'Inicie sesión para visualizar esta sección',
                    type: 'danger', 
                });
                $state.go('home', {});
                return false;
            }

            $scope.config = appConfig;
            $scope.users = [];
            $scope.user = null;
            $scope.pagination = {
                'total': null,
                'per_page': null,
                'current_page': null,
                'last_page': null
            };
            var emptyUser = {
                id: null,
                name: null,
                email: null
            };
            var configModal = {
                windowClass: 'am-fade-and-scale',
                backdrop: 'static',
                templateUrl: 'userFormModal.html',
                controller: 'userModalInstCtrl',
                resolve: {
                    params: function () {
                        return {
                            user: $scope.user
                        };
                    },
                    options: {
                        disabled: false // Set enable/disable the submit button
                    }
                }
            };
            
            $scope.list = function(page) {
                $scope.pagination.current_page = page || 1;
                Helper.addIconSpinner('#seccion_title');

                UsersService.getList({page: $scope.pagination.current_page}).then(function (users) {
                    // Copiamos los metadatos del paginador
                    $scope.pagination = _.extend($scope.pagination, _.pick(users.meta.extra, 'total', 'per_page', 'last_page'));
                    Helper.removeIconSpinner('#seccion_title');
                    $scope.users = users;
                }, function (response) {
                    Helper.removeIconSpinner('#seccion_title');
                });
            };

            $scope.create = function () {
                $scope.user = _.clone(emptyUser);
                configModal.resolve.options.disabled = false;
                var modalInstance = $uibModal.open(configModal);

                modalInstance.result.then(function (userModel) {
                        $scope.users.push(userModel);
                    }, function () {
                        //console.log('Modal closed');
                    }
                );
            };

            $scope.show = function (user, event) {
                Helper.addIconSpinner(event);

                UsersService.one(user.id).get().then(function(user) {
                    Helper.removeIconSpinner(event);
                    $scope.user = user;
                    configModal.resolve.options.disabled = true;
                    $uibModal.open(configModal);
                }, function (response) {
                    Helper.removeIconSpinner(event);
                });
            };

            $scope.edit = function (user, event) {
                Helper.addIconSpinner(event);

                UsersService.one(user.id).get().then(function(user) {
                    Helper.removeIconSpinner(event);
                    $scope.user = user;
                    configModal.resolve.options.disabled = false;
                    var modalInstance = $uibModal.open(configModal);

                    modalInstance.result.then(function (userModel) {
                            _.find($scope.users, function (element, index, list) {
                                if (element.id == userModel.id) {
                                    // Reemplaza el elemento actual con el elemento actualizado
                                    $scope.users[index] = userModel;

                                    return true; // Detiene la busqueda de elementos
                                }
                            });
                        }, function () {
                            //console.log('Modal closed');
                        }
                    );
                }, function (response) {
                    Helper.removeIconSpinner(event);
                });
            };

            $scope.delete = function (user, event) {
                confirm(function () {
                    Helper.addIconSpinner(event);

                    user.remove().then(function(response) {
                        Helper.removeIconSpinner(event);
                        $scope.users = _.without($scope.users, user);
                    }, function (response) {
                        Helper.removeIconSpinner(event);
                    });
                });
            };

            $scope.pageChanged = function () {
                $scope.list($scope.pagination.current_page);
            };

            // Iniciamos cargando el listado de usuarios
            $scope.list();
        }
    ]);

angular.module(Config.appName)
    .controller('userModalInstCtrl', ['$scope', '$uibModalInstance', 'params', 'options', 'UsersService',
        function ($scope, $uibModalInstance, params, options, UsersService) {
            $scope.userModel = params.user;
            $scope.disabled = options.disabled;
            var action = null;

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.submitForm = function () {
                if ($scope.formUser.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.formUser.$setSubmitted();
                    errorAlert('Revise los datos porporcionados');
                } else {
                    Helper.addIconSpinner('#btnSave');

                    if ($scope.userModel.id !== null) {
                        // Actualizar
                        action = $scope.userModel.put();
                    } else {
                        // Crear
                        action = UsersService.post($scope.userModel);
                    }

                    action.then(function (user) {
                        Helper.removeIconSpinner('#btnSave');
                        $uibModalInstance.close(user);
                    }, function (response) {
                        Helper.removeIconSpinner('#btnSave');
                    });
                }
            };
        }
    ]);