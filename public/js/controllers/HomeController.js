'use strict';
/* global angular */
/* global Helper */
/* global Config */

/**
 * @ngdoc function
 * @name mainApp.controllers:HomeController
 * @description
 * # HomeController
 * Controller of the mainApp
 */
angular.module(Config.appName)
    .controller('HomeController', ['$scope', function ($scope) {
        $scope.message = 'Hola Mundo...';
    }]);
