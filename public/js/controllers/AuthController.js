'use strict';
/* globals angular, Config, Helper, _, alert, confirm, console */

/**
 * @ngdoc function
 * @name mainApp.controllers:AuthController
 * @description
 * # AuthController
 * Controller of the mainApp
 */
angular.module(Config.appName)
    .controller('AuthController', ['$auth', '$state', '$alert', 'appConfig',
        function ($auth, $state, $alert, appConfig) {
            var vm = this;
            vm.config = appConfig;
                
            vm.login = function() {
                Helper.addIconSpinner('#btnLogin');

                var credentials = {
                    email: vm.email,
                    password: vm.password
                };
                
                // Use Satellizer's $auth service to login
                $auth.login(credentials).then(function(data) {
                    Helper.removeIconSpinner('#btnLogin');
                    // If login is successful, redirect to the users state
                    $state.go('users', {});
                }, function (response) {
                    Helper.removeIconSpinner('#btnLogin');
                    $alert({
                        title: 'Acceso denegado', 
                        content: 'Usuario o Contraseña incorrecto',
                        type: 'danger', 
                    });
                });
            };

            vm.logout = function() {
                $auth.logout();
                $state.go('home', {});
            };

            vm.isAuthenticated = function () {
                return $auth.isAuthenticated();
            };
        }
    ]);
