'use strict';
/* globals angular, Config, Helper, console, window, $, alert, errorAlert */

/**
 * @ngdoc overview
 * @name mainApp
 * @description
 * # mainApp
 *
 * Main module of the application.
 */
var mainApp = angular.module(Config.appName, [
        //'ngRoute',
        'ngAnimate',
        'ngTouch',
        'ngMessages',
        'ui.bootstrap', /* http://angular-ui.github.io/bootstrap/ */
        'mgcrea.ngStrap', /* http://mgcrea.github.io/angular-strap/ */
        'restangular', /* https://github.com/mgonto/restangular */
        'satellizer', /* https://github.com/sahat/satellizer */
        'ui.router', /* https://github.com/angular-ui/ui-router */
        'angular-loading-bar', /* https://github.com/chieffancypants/angular-loading-bar */
    ])
    // Register the Config as a angular constant
    .constant('appConfig', Config)
    .config(['$stateProvider', '$urlRouterProvider', '$authProvider', 'RestangularProvider', '$alertProvider', 'cfpLoadingBarProvider', 
    function ($stateProvider, $urlRouterProvider, $authProvider, RestangularProvider, $alertProvider, cfpLoadingBarProvider) { // $routeProvider
        /*$routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'HomeController'
            })
            .when('/users', {
                templateUrl: 'views/user/index.html',
                controller: 'UserController'
            })
            .otherwise({
                redirectTo: '/'
            });*/
        
        // The loading bar will only display after it has been waiting for a response for over 50ms
        cfpLoadingBarProvider.latencyThreshold = 50;
        
        // Satellizer configuration that specifies which API
        // route the JWT should be retrieved from
        $authProvider.loginUrl = Config.url_host + Config.url_auth;
        // set the token parent element if the token is not the JSON root
        $authProvider.tokenRoot = 'data';

        // Redirect to the home state if any other states are requested
        $urlRouterProvider.otherwise('/');
        
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/home.html',
                controller: 'HomeController'
            })
            .state('auth', {
                url: Config.url_login,
                templateUrl: 'views/auth/login.html',
                controller: 'AuthController as auth',
                resolve: {
                    skipIfLoggedIn: skipIfLoggedIn
                }
            })
            .state('users', {
                url: '/users',
                templateUrl: 'views/user/index.html',
                controller: 'UserController',
                resolve: {
                    loginRequired: loginRequired
                }
            });


        RestangularProvider.setBaseUrl(Config.url_host);

        RestangularProvider.setErrorInterceptor(function(response, deferred, responseHandler) {
            // Excepciones devueltos por jwt-auth -> JSON Web Token Authentication
            switch(response.status) {
                case 400: // TokenInvalidException, InvalidClaimException
                case 401: // TokenExpiredException, TokenBlacklistedException
                //case 500: // PayloadException, JWTException
                    // $location.path('/path1/path2');
                    // $route.reload();
                    // Elimiminamos el token actual que es inválido
                    // $auth.removeToken();
                    $.localStorage.remove($authProvider.tokenPrefix + '_' + $authProvider.tokenName);
                    //window.location.href = Config.url_web + '/#' + Config.url_login;
                    Helper.showError('ERROR: sesión expirada, inicie sesión nuevamente.');
                    window.location.reload();
                break;

                case 403: // Forbidden
                    Helper.showError(response.data.message || 'Acceso denegado');
                break;

                case 404: // Not Found
                    Helper.showError(response.data.message || 'Recurso no encontrado');
                break;

                case 422: // Validación de datos
                    Helper.showError(response.data.message);
                break;
            }

            if (Config.debug) {
                Helper.handleErrorResponse(response);
            }

            return true;  // error NO handled
        });

        RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
            var extractedData;
            /* Se recibe como respuesta este array, por lo que se tiene que 
               parsear para que pueda ser procesado por Restangular
            [
                // Datos extras que se requieran
                'extra'   => null,
                // Datos solicitados
                'data'    => null,
                // Mensaje breve descriptivo de la respuesta
                'message' => null,
                // Código de la respuesta HTTP
                'status'  => null,
                // Código del error
                'code' => null
            ]*/

            if (typeof data.data != 'undefined') {
                extractedData = data.data;
                // En meta almacenaremos el resto de valores, 
                // excepto data que es el contenido principal
                extractedData.meta = _.omit(data, 'data');
            } else {
                extractedData = data;
            }

            return extractedData;
        });

        // Satellizer -> JSON Web Tokens (JWT)
        // $authProvider.authHeader = Authorization 
        // $authProvider.authToken = Bearer 
        //RestangularProvider.setDefaultHeaders({Authorization: 'Bearer ' + $auth.getToken()});
        /*RestangularProvider.setFullRequestInterceptor(function(element, operation, route, url, headers, params, httpConfig) {
            return {
                element: element,
                params: params,
                headers: headers,
                httpConfig: httpConfig
            };
        });*/

        // Se establece los valores por defecto para el alert
        angular.extend($alertProvider.defaults, {
            animation: 'am-fade-and-scale',
            placement: 'top-right',
            container: 'section.main.container',
            keyboard: 'true',
            duration: 4,
            show: true
        });

        // Detiene la ejecución del controlador en caso de que el usuario este logueado
        function skipIfLoggedIn($q, $auth) {
            var deferred = $q.defer();
            
            if ($auth.isAuthenticated()) {
                deferred.reject();
            } else {
                deferred.resolve();
            }

            return deferred.promise;
        }

        // Para evitar errores al ejecutar uglify
        skipIfLoggedIn.$inject = ['$q', '$auth'];

        // Redirecciona a la pagina de login en caso de que el usuario NO este logueado
        function loginRequired($q, $location, $auth) {
            var deferred = $q.defer();
            
            if ($auth.isAuthenticated()) {
                deferred.resolve();
            } else {
                $location.path(Config.url_login);
            }

            return deferred.promise;
        }

        // Para evitar errores al ejecutar uglify
        loginRequired.$inject = ['$q', '$location', '$auth'];
        
    }])/*.run(['$rootScope','$location', function($rootScope, $location){
        locationInst = $location;
    }])*/;

//var locationInst = null;