var Helper = {
    /**
     * Show a content in console when the value of Config.debug is true
     *
     * @param mixed message Content to show in console, it could be a string or object
     */
    log: function(message) {
        if (Config.debug) {
            console.log(message);
        }
    },

    /**
     * Add icon spinner to the element
     * @param mixed source Object Event | CSS Selector
     * @depend jQuery
     */
    addIconSpinner: function (source) {
        if (typeof source != 'undefined') {
            // CSS Selector
            var el = source;

            // Event Object
            if (typeof source == 'object') {
                el = source.target || source.srcElement;
            }

            $(el).append(Config.icon_spinner);
        }
    },

    /**
     * Remove the icon spinner to the element
     * @param mixed source Object Event | CSS Selector
     * @depend jQuery
     */
    removeIconSpinner: function (source) {
        if (typeof source != 'undefined') {
            // CSS Selector
            var el = source;

            // Event Object
            if (typeof source == 'object') {
                el = source.target || source.srcElement;
            }

            $(el).find(Config.selector_icon_spinner)
                 .fadeOut('slow', function(){ $(this).remove(); });
        }
    },

    /**
     * Show information about the error on Restangular request
     * 
     * @param  object response
     */
    handleErrorResponse: function (response) {
        console.log(response);
        var info = 'status: ' + response.status + '\n' +
            'statusText: ' + response.statusText + '\n' +
            'url: ' + response.config.url + '\n' +
            'method: ' + response.config.method + '\n' +
            'data: ' + JSON.stringify(response.config.data) + '\n' +
            'responseText: ';

        if (typeof response.data == 'object') {
            info += JSON.stringify(response.data);
        } else if (response.data != ''){
            info += response.data.substr(0, 1100);
        }

        console.log(info);

        errorAlert(info.replace(new RegExp('\r?\n','g'), '<br />'));
    },

    /**
     * Muestra los mensajes de error en un errorAlert
     * @param  string|object message 
     */
    showError: function (message) {
        var info = '';

        if (typeof message == 'object') {
            info += _.reduce(message, function(memo, item) { 
                if (typeof item == 'object') {
                    return memo += _.reduce(item, function(acumulate, element) { 
                        return acumulate + element + ' \n'; 
                    }, '');
                } else {
                    return memo + item + ' \n';
                }
            }, '');
        } else {
            info += message;
        }
        
        errorAlert(info.replace(new RegExp('\r?\n','g'), '<br />'));
    },
};