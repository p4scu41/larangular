module.exports = function(grunt) {

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            config: {
                src: 'js/config.js',
                dest: 'js/config.min.js'
            },
            helper: {
                src: 'js/helper.js',
                dest: 'js/helper.min.js'
            },
            mainApp: {
                src: 'js/mainApp.js',
                dest: 'js/mainApp.min.js'
            },
            controllers: {
                src: 'js/controllers/*.js',
                dest: 'js/controllers.min.js'
            },
            services: {
                src: 'js/services/*.js',
                dest: 'js/services.min.js'
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed' // expanded
                },
                files: {
                    'css/app.min.css': '../resources/assets/sass/app.scss',
                }
            }
        },

        /*cssmin: {
            dist: {
                files: {
                    'css/app.min.css': ['css/app.css']
                }
            }
        },*/

        watch: {
            js: {
                files: ['js/**/*.js'],
                tasks: ['uglify']
            },
            //css: {
            //    files: ['css/**/*.css'],
            //    tasks: ['cssmin']
            //},
            styles: {
                files: ['../resources/assets/sass/app.scss'],
                tasks: ['sass']
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'sass',
        //'cssmin',
        'uglify',
        'watch'
    ]);
};
