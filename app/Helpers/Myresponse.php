<?php
namespace App\Helpers;
/**
* 
*/
class MyResponse
{
    public static $json_response = [
        // Datos extras que se requieran
        'extra'   => null,
        // Datos solicitados
        'data'    => null,
        // Mensaje breve descriptivo de la respuesta
        'message' => null,
        // Código de la respuesta HTTP
        'status'  => null,
        // Código del error
        'error' => null
    ];
    
    public static function json($data, $status = 200, $headers = [])
    {
        $content = array_merge(self::$json_response, $data);

        if (empty($content['status'])) {
            $content['status'] = $status;
        }

        return response()->json($content, $status); 
    }

    public static function jsonNotFound($data = [], $headers = [])
    {
        $status = 404;
        $content = array_merge(self::$json_response, $data);

        if (empty($content['error'])) {
            $content['error'] = $status;
        }

        if (empty($content['message'])) {
            $content['message'] = 'Recurso no encontrado';
        }        

        return response()->json($content, $status, $headers); 
    }

    public static function jsonForbidden($data = [], $headers = [])
    {
        $status = 403;
        $content = array_merge(self::$json_response, $data);

        if (empty($content['error'])) {
            $content['error'] = $status;
        }

        if (empty($content['message'])) {
            $content['message'] = 'Acceso no autorizado';
        }        

        return response()->json($content, $status, $headers); 
    }
}
