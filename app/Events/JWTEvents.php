<?php

namespace App\Events;

use App\Events\Event;
use App\Helpers\Myresponse;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Eventos lanzados por jwt-auth -> JSON Web Token Authentication
 */
class JWTEvents extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }

    public function valid($user)
    {
        
    }

    public function user_not_found($e)
    {
        return Myresponse::json([
            'error' => $e->getStatusCode(),
            'message' => 'Usuario no encontrado',
            'extra' => $e->getMessage(),
        ], $e->getStatusCode());
    }

    public function invalid($e)
    {
        return Myresponse::json([
            'error' => $e->getStatusCode(),
            'message' => 'Token inválido',
            'extra' => $e->getMessage(),
        ], $e->getStatusCode());
    }

    public function expired($e)
    {
        return Myresponse::json([
            'error' => $e->getStatusCode(),
            'message' => 'Token expirado',
            'extra' => $e->getMessage(),
        ], $e->getStatusCode());
    }

    public function absent()
    {
        return Myresponse::json([
            'error' => 400,
            'message' => 'Token ausente',
            'extra' => 'token_absent',
        ], 400);
    }
}
