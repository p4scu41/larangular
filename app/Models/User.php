<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * Disable the attributes created_at and updated_at
     * @var boolean
     */
    public $timestamps = false; 

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'type', 'active'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:10',
        'email' => 'required|max:15|email',
    ];

    /**
     * Custom messages for validator errors
     *
     * @var array
     */
    public static $messages = [
        /*'name.required' => 'El nombre es obligatorio',
        'email.required' => 'El e-mail es obligatorio',*/
    ];

    public function isSuperAdmin()
    {
        return $this->type === 1;
    }

    public function isAdmin()
    {
        return $this->type === 2;
    }

    public function isStudent()
    {
        return $this->type === 3;
    }

}
