<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Helpers\MyResponse;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // Solo prodran loguearse usuarios activos
        $credentials['active'] = 1;

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                // Utilizamos la clase personalizada para devolver la respuesta
                return MyResponse::json([
                    'error' => 'invalid_credentials',
                    'message' => 'Usuario o Contraseña incorrecta'
                ], 401);
                //return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            // Utilizamos la clase personalizada para devolver la respuesta
            return MyResponse::json([
                'error' => 'could_not_create_token',
                'message' => 'Error al iniciar sesión. Intentelo nuevamente.',
                'extra' => $e->getMessage()
            ], 500);
            //return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        //return response()->json(compact('token'));
        return MyResponse::json([
            'data' => ['token' => $token]
        ]);
    }
}
