<?php

namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Response;
use App\Http\Controllers\Controller;
use App\Helpers\MyResponse;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Route; // Para poder pasar el parametro $route al metodo find que se ejecuta en el beforeFilter
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public $user = null;

    function __construct()
    {
        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);

        $this->middleware('user.admin', ['except' => ['authenticate']]);

        $this->beforeFilter('@find',['only' => ['show','edit','update', 'destroy']]);
    }

    /**
     * Get de user with the specific ID
     * @param  Route  $route 
     * @return void   Redirect when the post is not found
     */
    public function find(Route $route)
    {
        // En ocasiones el parametro enviado es el nombre del recurso y en otras es el id
        $id = $route->getParameter('users') ? $route->getParameter('users') : $route->getParameter('id');
        $this->user = User::find($id);

        if (empty($this->user)) {
            return MyResponse::jsonNotFound();
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->input('page');

        // Si se esta utilizando paginación
        if ($page) {
            $paginator = User::paginate(10)->toArray();
            // Obtenemos el listado de usuarios
            $users = $paginator['data'];
            // Eliminamos los datos del paginador, 
            // para enviar solo los metadatos de la paginación
            unset($paginator['data']);
            unset($paginator['next_page_url']);
            unset($paginator['prev_page_url']);
        } else {
            $paginator = null;
            $users = User::all();
        }

        return MyResponse::json([
            'data' => $users,
            'extra' => $paginator,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->all()); 
        return MyResponse::json([
            'data' => $user->toArray()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return MyResponse::json([
            'data' => $this->user->toArray()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $this->user->fill($request->all());
        $this->user->save();
        return MyResponse::json([
            'data' => $this->user->toArray()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->user->toArray();
        $this->user->delete();
        // Cuando eliminamos devolvemos el elemento que se eliminó
        return MyResponse::json([
            'data' => $data
        ]);
    }
}
