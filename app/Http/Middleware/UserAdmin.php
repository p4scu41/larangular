<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\MyResponse;
use Tymon\JWTAuth\JWTAuth;

class UserAdmin
{
    protected $user;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $this->auth->setRequest($request)->getToken();
        $user = $this->auth->authenticate($token);
        // En los controladores, se puede obtener el usuario con
        // $user = JWTAuth::parseToken()->authenticate();

        if (!$user->isSuperAdmin()) {
            return MyResponse::jsonForbidden();
        }

        return $next($request);
    }
}
