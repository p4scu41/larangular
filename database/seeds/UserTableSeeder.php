<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    // NOTA: En caso de cambiar el nombre de la clase despues de crearlo con
    // php artisan make:seeder UserTableSeeder 
    // Ejecutar el siguiente comando: 
    // composer dump-autoload
    // para que laravel reconozca el nuevo nombre de la clase,
    // es decir, que lo agregue al class map

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=0; $i<100; $i++) {
            DB::table('users')->insert([
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'password' => Hash::make('123'),
                'type' => rand(1, 3),
            ]);
        }
    }
}
